import React, {useState} from 'react';
import { StyleSheet, Text, View, Button, Alert } from 'react-native';
import Contacts from 'react-native-contacts';

export default function App() {
  const [output, change] = useState('Open up App.js to start working on your app!')
  var newPerson = {
    emailAddresses: [{
      label: "work",
      email: "authur.dent@example.com",
    }],
    phoneNumbers: [{
      label: 'Work', 
      number:'111-111-1142'},
    ],
    familyName: "Dent",
    givenName: "Matthew",
  }
  
  // Contacts.checkPermission((err, permission) => {
  //   if (err) throw err;
  
  //   // Contacts.PERMISSION_AUTHORIZED || Contacts.PERMISSION_UNDEFINED || Contacts.PERMISSION_DENIED
  //   if (permission === 'undefined') {
  //     Contacts.requestPermission((err, permission) => {
  //       // ...
  //     })
  //   }
  //   if (permission === 'authorized') {
  //     // yay!
  //   }
  //   if (permission === 'denied') {
  //     // x.x
  //   }
  // })
  return (
    <View style={styles.container}>
      <Text>{output}</Text>
      <Button 
          title="Press me"
          style = {styles.button1}
          onPress ={()=>{Contacts.addContact(newPerson, (err) => {
            if (err) throw err;
            // save successful
          })}}/>

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button1:{
    color: '#f194ff'

  }
});
